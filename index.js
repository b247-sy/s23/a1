
/* 

3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)

*/

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	} 
}

console.log(trainer);

/*6. Access the trainer object properties using dot and square bracket notation.*/

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

/*7. Invoke/call the trainer talk object method.*/

console.log("Result of talk method:");
trainer.talk();

/*

8. Create a constructor for creating a pokemon with the following properties:
Name (Provided as an argument to the contructor)
Level (Provided as an argument to the contructor)
Health (Create an equation that uses the level property)
Attack (Create an equation that uses the level property) */

/*10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.*/

function pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + " health is now reduced to " + Number(target.health - this.attack));
		target.health = target.health - this.attack;
		if (target.health <= 0){
			this.faint(target);
		}
	}
	this.faint = function(target){
				console.log(target.name + " fainted. ");
		};
};

/*9. Create/instantiate several pokemon object from the constructor with varying name and level properties.*/

let pikachu = new pokemon("Pikachu", 12);
console.log(pikachu);

let geodue = new pokemon("Geodue", 8);
console.log(geodue);

let mewtwo = new pokemon("Mewtwo", 100);
console.log(mewtwo);

geodue.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodue);

console.log(geodue);